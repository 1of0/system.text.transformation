﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("System.Text.Transformation")]
[assembly: AssemblyDescription("Provides various extensions to perform transformations on text")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("1of0")]
[assembly: AssemblyProduct("System.Text.Transformation")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("9a957b78-6fb3-41d7-b950-7ba2fa4d8c84")]
[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
