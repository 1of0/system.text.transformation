﻿using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace System.Text.Transformation
{
	/// <summary>
	/// Provides extension methods to perform transformations between various text styles; e.g. camel case, pascal case.
	/// </summary>
	public static class CaseConverter
	{
		/// <summary>
		/// Holds a collection of detectors for each text style
		/// </summary>
		private static readonly Dictionary<CaseType, Regex> Detectors = new Dictionary<CaseType, Regex>
		{
			{ CaseType.PascalCase, new Regex("^(?:[A-Z][a-z]*)+$", RegexOptions.Compiled) },
			{ CaseType.TitleCase, new Regex("^(?:[A-Z][a-z]*\\s+)+[A-Z][a-z]*$", RegexOptions.Compiled) },
			{ CaseType.FirstWordCase, new Regex("^[A-Z][a-z]*(?:\\s+[a-z]*)+$", RegexOptions.Compiled) },
			{ CaseType.CamelCase, new Regex("^[a-z]+(?:[A-Z][a-z]*)+$", RegexOptions.Compiled) },
			{ CaseType.HyphenCase, new Regex("^(?:[a-z]+-)+[a-z]+$", RegexOptions.Compiled) },
			{ CaseType.UpperHyphenCase, new Regex("^(?:[A-Z]+-)+[A-Z]+$", RegexOptions.Compiled) },
			{ CaseType.SnakeCase, new Regex("^(?:[a-z]+_)+[a-z]+$", RegexOptions.Compiled) },
			{ CaseType.UpperSnakeCase, new Regex("^(?:[A-Z]+_)+[A-Z]+$", RegexOptions.Compiled) }
		};

		/// <summary>
		/// Attempts to detect a particular text style given the provide <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to analyze</param>
		/// <returns>Detected text style</returns>
		public static CaseType DetectCaseType(this string input)
		{
			return Detectors.FirstOrDefault(detector => detector.Value.IsMatch(input)).Key;
		}

		/// <summary>
		/// Detects the text style of the provided <paramref name="input"/> and splits the text by the boundaries that
		/// are specific to the relevant text style. If the text style can not be detected, white space will be assumed
		/// as boundary.
		/// </summary>
		/// <param name="input">Text to convert to words</param>
		/// <returns>Collection of words split by style-specific boundaries</returns>
		public static IEnumerable<string> ToWords(this string input)
		{
			switch (DetectCaseType(input))
			{
				case CaseType.PascalCase:
				case CaseType.CamelCase:

					return input.ToWords(Char.IsUpper, false);


				case CaseType.HyphenCase:
				case CaseType.UpperHyphenCase:

					return input.ToWords(c => c == '-', true);

				case CaseType.SnakeCase:
				case CaseType.UpperSnakeCase:

					return input.ToWords(c => c == '_', true);

				case CaseType.TitleCase:
				case CaseType.FirstWordCase:
				default:

					return input.ToWords(Char.IsWhiteSpace, true);
			}
		}

		/// <summary>
		/// Applies the provided <paramref name="transformation"/> action over the provide <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to transform</param>
		/// <param name="transformation">Transformation to apply</param>
		/// <returns>Transformed text</returns>
		public static string Transform(this string input, Action<List<char>> transformation)
		{
			List<char> buffer = input.ToList();
			transformation(buffer);
			return new string(buffer.ToArray());
		}

		/// <summary>
		/// Combines the provided <paramref name="words"/> into a string, optionally using the provided 
		/// <paramref name="glue"/> to join the words.
		/// </summary>
		/// <param name="words">Words to combine</param>
		/// <param name="glue">Glue to place between the words</param>
		/// <returns>String of combined words</returns>
		public static string Combine(this IEnumerable<string> words, string glue = "")
		{
			return words.Aggregate((s1, s2) => s1 + glue + s2);
		}

		/// <summary>
		/// Returns a PascalCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>PascalCase representation</returns>
		public static string ToPascalCase(this string input)
		{
			return input
				.ToWords()
				.Select(word => word.Transform(buffer => buffer[0] = Char.ToUpper(buffer[0])))
				.Combine()
			;
		}

		/// <summary>
		/// Returns a TitleCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>TitleCase representation</returns>
		public static string ToTitleCase(this string input)
		{
			return input
				.ToWords()
				.Select(word => word.Transform(buffer => buffer[0] = Char.ToUpper(buffer[0])))
				.Combine(" ")
			;
		}

		/// <summary>
		/// Returns a FirstWordCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>FirstWordCase representation</returns>
		public static string ToFirstWordCase(this string input)
		{
			return input
				.ToWords()
				.Select((word, index) =>
					(index == 0)
						? word.Transform(buffer => buffer[0] = Char.ToUpper(buffer[0]))
						: word
				)
				.Combine(" ")
			;
		}

		/// <summary>
		/// Returns a CamelCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>CamelCase representation</returns>
		public static string ToCamelCase(this string input)
		{
			return input
				.ToWords()
				.Select((word, index) =>
					(index > 0)
						? word.Transform(buffer => buffer[0] = Char.ToUpper(buffer[0]))
						: word
				)
				.Combine()
			;
		}

		/// <summary>
		/// Returns a SnakeCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>SnakeCase representation</returns>
		public static string ToSnakeCase(this string input)
		{
			return input.ToWords().Combine("_");
		}

		/// <summary>
		/// Returns a UpperSnakeCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>UpperSnakeCase representation</returns>
		public static string ToUpperSnakeCase(this string input)
		{
			return input.ToWords().Select(word => word.ToUpper()).Combine("_");
		}

		/// <summary>
		/// Returns a HyphenCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>HyphenCase representation</returns>
		public static string ToHyphenCase(this string input)
		{
			return input.ToWords().Combine("-");
		}

		/// <summary>
		/// Returns a UpperHyphenCase representation of the provided <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <returns>UpperHyphenCase representation</returns>
		public static string ToUpperHyphenCase(this string input)
		{
			return input.ToWords().Select(word => word.ToUpper()).Combine("-");
		}

		/// <summary>
		/// Returns a representation of the provided <paramref name="caseType"/> applied on the provided 
		/// <paramref name="input"/>.
		/// </summary>
		/// <param name="input">Text to convert</param>
		/// <param name="caseType">Case type to apply</param>
		/// <returns>Cased representation</returns>
		public static string ToCase(this string input, CaseType caseType)
		{
			switch (caseType)
			{
				case CaseType.PascalCase: return input.ToPascalCase();
				case CaseType.CamelCase: return input.ToCamelCase();
				case CaseType.TitleCase: return input.ToTitleCase();
				case CaseType.FirstWordCase: return input.ToFirstWordCase();
				case CaseType.SnakeCase: return input.ToSnakeCase();
				case CaseType.UpperSnakeCase: return input.ToUpperSnakeCase();
				case CaseType.HyphenCase: return input.ToHyphenCase();
				case CaseType.UpperHyphenCase: return input.ToUpperHyphenCase();
			}

			return input;
		}

		/// <summary>
		/// Internal overload of the <see cref="ToWords(string)"/> method that splits the provided 
		/// <paramref name="input"/> into a collection of words, with the boundary defined by the provided 
		/// <paramref name="boundary"/> predicate. Also the behaviour on whether to include the boundary in the words
		/// can be controlled with the <paramref name="skipBoundary"/> parameter.
		/// </summary>
		/// <param name="input">Text to convert to words</param>
		/// <param name="boundary">Predicate on a character that defines whether it is a boundary</param>
		/// <param name="skipBoundary">Boolean value indicating whether or not boundaries are included in the words
		/// </param>
		/// <returns>Collection of words split by specified boundaries</returns>
		private static IEnumerable<string> ToWords(this string input, Predicate<char> boundary,
			bool skipBoundary)
		{
			List<string> words = new List<string>();
			string buffer = "";

			foreach (char c in input)
			{
				if (boundary(c) && buffer != "")
				{
					words.Add(buffer.ToLower());
					buffer = "";

					if (skipBoundary)
					{
						continue;
					}
				}
				buffer += c;
			}

			if (buffer != "")
			{
				words.Add(buffer.ToLower());
			}

			return words;
		}
	}

	/// <summary>
	/// Describes a text style
	/// </summary>
	public enum CaseType
	{
		/// <summary>
		/// Unknown style
		/// </summary>
		Unknown,
		/// <summary>
		/// Pascal case; e.g. SomeVariable
		/// </summary>
		PascalCase,
		/// <summary>
		/// Title case; e.g. Some Words
		/// </summary>
		TitleCase,
		/// <summary>
		/// First word case; e.g. Some plain words
		/// </summary>
		FirstWordCase,
		/// <summary>
		/// Camel case; e.g. someVariable
		/// </summary>
		CamelCase,
		/// <summary>
		/// Hyphen case; e.g. some-hyphenated-phrase
		/// </summary>
		HyphenCase,
		/// <summary>
		/// Upper hyphen case; e.g. SOME-HYPHENATED-PHRASE
		/// </summary>
		UpperHyphenCase,
		/// <summary>
		/// Snake case; e.g. some_variable
		/// </summary>
		SnakeCase,
		/// <summary>
		/// Upper snake case; e.g. UGLY_CONSTANTS
		/// </summary>
		UpperSnakeCase
	}
}
