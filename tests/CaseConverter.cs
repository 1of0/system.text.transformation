﻿using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;

namespace System.Text.Transformation.Tests
{
	public class CaseConverter
	{
		private static readonly Dictionary<string, CaseType> DetectionVectors = new Dictionary<string, CaseType>
		{
			{ "PascalCase", CaseType.PascalCase },
			{ "PascalCaseFourParts", CaseType.PascalCase },
			{ "camelCase", CaseType.CamelCase },
			{ "camelCaseFourParts", CaseType.CamelCase },
			{ "snake_case", CaseType.SnakeCase },
			{ "snake_case_four_parts", CaseType.SnakeCase },
			{ "UPPER_SNAKE_CASE", CaseType.UpperSnakeCase },
			{ "UPPER_SNAKE_CASE_FIVE_PARTS", CaseType.UpperSnakeCase },
			{ "hyphen-case", CaseType.HyphenCase },
			{ "hyphen-case-four-parts", CaseType.HyphenCase },
			{ "UPPER-HYPHEN-CASE", CaseType.UpperHyphenCase },
			{ "UPPER-HYPHEN-CASE-FIVE-PARTS", CaseType.UpperHyphenCase },
			{ "Title Case", CaseType.TitleCase },
			{ "Title Case Four Parts", CaseType.TitleCase },
			{ "First word case", CaseType.FirstWordCase },
			{ "First word case five parts", CaseType.FirstWordCase },
			{ "assorted words", CaseType.Unknown },
			{ "assorted words four parts", CaseType.Unknown }
		};

		private static readonly string TransformationVectorBase = "this is a simple sentence";

		private static readonly Dictionary<CaseType, string> TransformationVectors = new Dictionary<CaseType, string>
		{
			{ CaseType.PascalCase, "ThisIsASimpleSentence" },
			{ CaseType.CamelCase, "thisIsASimpleSentence" },
			{ CaseType.SnakeCase, "this_is_a_simple_sentence" },
			{ CaseType.HyphenCase, "this-is-a-simple-sentence" },
			{ CaseType.UpperSnakeCase, "THIS_IS_A_SIMPLE_SENTENCE" },
			{ CaseType.UpperHyphenCase, "THIS-IS-A-SIMPLE-SENTENCE" },
			{ CaseType.TitleCase, "This Is A Simple Sentence" },
			{ CaseType.FirstWordCase, "This is a simple sentence" },
			{ CaseType.Unknown, "this is a simple sentence" }
		};

		[Test]
		public void TestDection()
		{
			foreach (var vector in DetectionVectors)
			{
				Assert.AreEqual(vector.Value, vector.Key.DetectCaseType(), "Detection failed");
			}
		}

		[Test]
		public void TestToWords()
		{
			string simpleString = "just a bunch of words";
			var expectedWords = new[] { "just", "a", "bunch", "of", "words" };

			Assert.That(simpleString.ToWords().Count() == 5, "ToWords() does not produce expected number of words");
			Assert.That(simpleString.ToWords().SequenceEqual(expectedWords), "ToWords() does not produce expected sequence");

			Assert.That("JustABunchOfWords".ToWords().SequenceEqual(expectedWords), "ToWords() not working for PascalCase string");
			Assert.That("justABunchOfWords".ToWords().SequenceEqual(expectedWords), "ToWords() not working for camelCase string");
			Assert.That("just_a_bunch_of_words".ToWords().SequenceEqual(expectedWords), "ToWords() not working for SnakeCase string");
			Assert.That("just-a-bunch-of-words".ToWords().SequenceEqual(expectedWords), "ToWords() not working for HyphenCase string");
			Assert.That("JUST_A_BUNCH_OF_WORDS".ToWords().SequenceEqual(expectedWords), "ToWords() not working for UpperSnakeCase string");
			Assert.That("JUST-A-BUNCH-OF-WORDS".ToWords().SequenceEqual(expectedWords), "ToWords() not working for UpperHyphenCase string");
			Assert.That("Just A Bunch Of Words".ToWords().SequenceEqual(expectedWords), "ToWords() not working for TitleCase string");
			Assert.That("Just a bunch of words".ToWords().SequenceEqual(expectedWords), "ToWords() not working for FirstWordCase string");
		}

		[Test]
		public void TestCombine()
		{
			var words = new[] { "just", "a", "bunch", "of", "words" };
			Assert.AreEqual("justabunchofwords", words.Combine(), "Combine failed without glue");
			Assert.AreEqual("just-a-bunch-of-words", words.Combine("-"), "Combine failed with glue");
		}

		[Test]
		public void TestToCase()
		{
			foreach (var vector in TransformationVectors)
			{
				Assert.AreEqual(vector.Value, TransformationVectorBase.ToCase(vector.Key), "ToCase() failed");
			}
		}

		[Test]
		public void TestTransformations()
		{
			Assert.AreEqual(TransformationVectors[CaseType.PascalCase], TransformationVectorBase.ToPascalCase(), "ToPascalCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.CamelCase], TransformationVectorBase.ToCamelCase(), "ToCamelCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.SnakeCase], TransformationVectorBase.ToSnakeCase(), "ToSnakeCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.HyphenCase], TransformationVectorBase.ToHyphenCase(), "ToHyphenCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.UpperSnakeCase], TransformationVectorBase.ToUpperSnakeCase(), "ToUpperSnakeCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.UpperHyphenCase], TransformationVectorBase.ToUpperHyphenCase(), "ToUpperHyphenCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.TitleCase], TransformationVectorBase.ToTitleCase(), "ToTitleCase() failed");
			Assert.AreEqual(TransformationVectors[CaseType.FirstWordCase], TransformationVectorBase.ToFirstWordCase(), "ToFirstWordCase() failed");
		}
	}
}
